Require Export prosa.analysis.facts.busy_interval.pi_bound.
Require Export prosa.results.fixed_priority.rta.bounded_pi.
Require Export prosa.model.schedule.work_conserving.
Require Export prosa.analysis.definitions.busy_interval.
Require Export prosa.analysis.definitions.blocking_bound_fp.

(** * RTA for FP-schedulers with Bounded Non-Preemptive Segments *)

(** In this section we instantiate the Abstract RTA for FP-schedulers
    with Bounded Priority Inversion to FP-schedulers for ideal
    uni-processor model of real-time tasks with arbitrary
    arrival models _and_ bounded non-preemptive segments. *)

(** Recall that Abstract RTA for FP-schedulers with Bounded Priority
    Inversion does not specify the cause of priority inversion. In
    this section, we prove that the priority inversion caused by
    execution of non-preemptive segments is bounded. Thus the Abstract
    RTA for FP-schedulers is applicable to this instantiation. *)
Section RTAforFPwithBoundedNonpreemptiveSegmentsWithArrivalCurves.

  (** Consider any type of tasks ... *)
  Context {Task : TaskType}.
  Context `{TaskCost Task}.
  Context `{TaskRunToCompletionThreshold Task}.
  Context `{TaskMaxNonpreemptiveSegment Task}.

  (**  ... and any type of jobs associated with these tasks. *)
  Context {Job : JobType}.
  Context `{JobTask Job Task}.
  Context `{Arrival : JobArrival Job}.
  Context `{Cost : JobCost Job}.

  (** Consider an FP policy that indicates a higher-or-equal priority
      relation, and assume that the relation is reflexive and
      transitive. *)
  Context {FP : FP_policy Task}.
  Hypothesis H_priority_is_reflexive : reflexive_task_priorities FP.
  Hypothesis H_priority_is_transitive : transitive_task_priorities FP.

  (** Consider any arrival sequence with consistent, non-duplicate arrivals. *)
  Variable arr_seq : arrival_sequence Job.
  Hypothesis H_valid_arrival_sequence : valid_arrival_sequence arr_seq.

  (** Next, consider any ideal uni-processor schedule of this arrival sequence, ... *)
  Variable sched : schedule (ideal.processor_state Job).

  (** ... allow for any work-bearing notion of job readiness, ... *)
  Context `{@JobReady Job (ideal.processor_state Job) Cost Arrival}.
  Hypothesis H_job_ready : work_bearing_readiness arr_seq sched.

  (** ... and assume that the schedule is valid.  *)
  Hypothesis H_sched_valid : valid_schedule sched arr_seq.

  (** In addition, we assume the existence of a function mapping jobs
      to their preemption points ... *)
  Context `{JobPreemptable Job}.

  (** ... and assume that it defines a valid preemption
      model with bounded non-preemptive segments. *)
  Hypothesis H_valid_model_with_bounded_nonpreemptive_segments:
    valid_model_with_bounded_nonpreemptive_segments arr_seq sched.

  (** Next, we assume that the schedule is a work-conserving schedule... *)
  Hypothesis H_work_conserving : work_conserving arr_seq sched.

  (** ... and the schedule respects the scheduling policy. *)
  Hypothesis H_respects_policy : respects_FP_policy_at_preemption_point arr_seq sched FP.

  (** Assume we have sequential tasks, i.e, jobs from the
      same task execute in the order of their arrival. *)
  Hypothesis H_sequential_tasks : sequential_tasks arr_seq sched.

  (** Consider an arbitrary task set ts, ... *)
  Variable ts : list Task.

  (** ... assume that all jobs come from the task set, ... *)
  Hypothesis H_all_jobs_from_taskset : all_jobs_from_taskset arr_seq ts.

  (** ... and the cost of a job cannot be larger than the task cost. *)
  Hypothesis H_valid_job_cost:
    arrivals_have_valid_job_costs arr_seq.

  (** Let max_arrivals be a family of valid arrival curves, i.e., for
     any task [tsk] in ts [max_arrival tsk] is (1) an arrival bound of
     [tsk], and (2) it is a monotonic function that equals 0 for the
     empty interval delta = 0. *)
  Context `{MaxArrivals Task}.
  Hypothesis H_valid_arrival_curve : valid_taskset_arrival_curve ts max_arrivals.
  Hypothesis H_is_arrival_curve : taskset_respects_max_arrivals arr_seq ts.

  (** Let [tsk] be any task in ts that is to be analyzed. *)
  Variable tsk : Task.
  Hypothesis H_tsk_in_ts : tsk \in ts.

  (** Consider a valid preemption model... *)
  Hypothesis H_valid_preemption_model:
    valid_preemption_model arr_seq sched.

  (** ...and a valid task run-to-completion threshold function. That
     is, [task_rtct tsk] is (1) no bigger than [tsk]'s cost, (2) for
     any job of task [tsk] [job_rtct] is bounded by [task_rtct]. *)
  Hypothesis H_valid_run_to_completion_threshold:
    valid_task_run_to_completion_threshold arr_seq tsk.

  (** Let's define some local names for clarity. *)
  Let task_rbf := task_request_bound_function tsk.
  Let total_hep_rbf := total_hep_request_bound_function_FP ts tsk.
  Let total_ohep_rbf := total_ohep_request_bound_function_FP ts tsk.
  Let response_time_bounded_by := task_response_time_bound arr_seq sched.

  (** ** Priority inversion is bounded *)
  (** In this section, we prove that a priority inversion for task [tsk] is bounded by
      the maximum length of non-preemptive segments among the tasks with lower priority. *)
  Section PriorityInversionIsBounded.

    (** First, we prove that the maximum length of a priority inversion of a job j is
       bounded by the maximum length of a non-preemptive section of a task with
       lower-priority task (i.e., the blocking term). *)
    Lemma priority_inversion_is_bounded_by_blocking:
      forall j t1 t2,
        arrives_in arr_seq j ->
        job_of_task tsk j ->
        busy_interval_prefix arr_seq sched j t1 t2 ->
        max_lp_nonpreemptive_segment arr_seq j t1 <= blocking_bound ts tsk.
    Proof.
      move=> j t1 t2 ARR TSK BUSY; move: TSK => /eqP TSK.
      rewrite /blocking_bound /max_lp_nonpreemptive_segment.
      apply: leq_trans; first exact: max_np_job_segment_bounded_by_max_np_task_segment.
      apply: (@leq_trans (\max_(j_lp <- arrivals_between arr_seq 0 t1
                | (~~ hep_task (job_task j_lp) tsk) && (0 < job_cost j_lp))
                            (task_max_nonpreemptive_segment (job_task j_lp) - ε))).
      { rewrite /hep_job /FP_to_JLFP TSK.
        apply: leq_big_max => j' JINB NOTHEP.
        rewrite leq_sub2r //. }
      { apply /bigmax_leq_seqP => j' JINB /andP[NOTHEP POS].
        apply leq_bigmax_cond_seq with
            (x := (job_task j')) (F := fun tsk => task_max_nonpreemptive_segment tsk - 1);
          last by done.
        apply: H_all_jobs_from_taskset.
        by apply: in_arrivals_implies_arrived (JINB). }
    Qed.

    (** Using the above lemma, we prove that the priority inversion of the task
        is bounded by the blocking_bound. *)
    Lemma priority_inversion_is_bounded:
      priority_inversion_is_bounded_by
        arr_seq sched tsk (constant (blocking_bound ts tsk)).
    Proof.
      have PIB: priority_inversion_is_bounded_by arr_seq sched tsk (fun=> blocking_bound ts tsk); last by done.
      apply: priority_inversion_is_bounded => //.
      by exact: priority_inversion_is_bounded_by_blocking.
    Qed.

  End PriorityInversionIsBounded.

  (** ** Response-Time Bound *)
  (** In this section, we prove that the maximum among the solutions of the response-time
      bound recurrence is a response-time bound for [tsk]. *)
  Section ResponseTimeBound.

    (** Let L be any positive fixed point of the busy interval recurrence. *)
    Variable L : duration.
    Hypothesis H_L_positive : L > 0.
    Hypothesis H_fixed_point : L = blocking_bound ts tsk + total_hep_rbf L.

    (** To reduce the time complexity of the analysis, recall the notion of search space. *)
    Let is_in_search_space := is_in_search_space tsk L.

    (** Next, consider any value R, and assume that for any given arrival offset A from the search
       space there is a solution of the response-time bound recurrence that is bounded by R. *)
    Variable R : duration.
    Hypothesis H_R_is_maximum:
      forall (A : duration),
        is_in_search_space A ->
        exists (F : duration),
          A + F >= blocking_bound ts tsk
                  + (task_rbf (A + ε) - (task_cost tsk - task_rtct tsk))
                  + total_ohep_rbf (A + F) /\
          F + (task_cost tsk - task_rtct tsk) <= R.

    (** Then, using the results for the general RTA for FP-schedulers, we establish a
       response-time bound for the more concrete model of bounded nonpreemptive segments.
       Note that in case of the general RTA for FP-schedulers, we just _assume_ that
       the priority inversion is bounded. In this module we provide the preemption model
       with bounded nonpreemptive segments and _prove_ that the priority inversion is
       bounded. *)
    Theorem uniprocessor_response_time_bound_fp_with_bounded_nonpreemptive_segments:
      response_time_bounded_by tsk R.
    Proof.
      eapply uniprocessor_response_time_bound_fp;
        eauto using priority_inversion_is_bounded with basic_rt_facts.
    Qed.

  End ResponseTimeBound.

End RTAforFPwithBoundedNonpreemptiveSegmentsWithArrivalCurves.
